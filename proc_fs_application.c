/*
 *	Dev: 	BramVL
 *	Date:	20/12/2013
 *	year:	2013 - 2014
 */

#include "can_lib.h"

#define FILENAME "/proc/ADC_MODULE/status"
#define FILEMODE "r"

int main(int argc,char *argv[]) {
	//******************** CHECK ARGUMENTS *********************//
	if(argc < 2){
		printf("Usage: ./proc_app <loopback 1/0>\n");
		exit(-1);
	}
	//**********************************************************//

	//*********************** VARS *****************************//
	int enableLoopback;		// Enable loopback
	__u8 readBuffer[8]; 		// CAN read buffer
	__u8 WriteBuffer[8];		// CAN write buffer
	struct sockaddr_can sockAddr;	// CAN struct
	int socket,errorStatus;		// Socket and error ints
	struct ifreq ifr;		// ifr struct
	FILE *fp;			// File pointer
	char adcValueBuffer[8];		// Buffer for the ADC
	int adcValue = 0;		// ADC value (proc_fs)
	int prevAdcValue = 1;		// Temp var for ADC value
	//**********************************************************//

	//************************ CAN INIT ************************//
	enableLoopback = atoi(argv[1]);

	errorStatus = load_can_modules(enableLoopback);
	error_report(errorStatus);

	errorStatus = init_can_socket(&sockAddr,&socket,&ifr,
			enableLoopback);
	error_report(errorStatus);

	errorStatus = bind_can_socket(&socket,sockAddr);
	error_report(errorStatus);
	//*********************************************************//

	//********************** FILE INIT ************************//
	
	//*********************************************************//

	//********************* MAIN LOOP *************************//
	while(1){
		fp = fopen(FILENAME,FILEMODE);
		if(fp == NULL)
			error_report(-1);
		//printf("#INFO: File %s opened\n",FILENAME);
		if(adcValue < prevAdcValue -2 || adcValue > prevAdcValue +2){
			prevAdcValue = adcValue;
			printf("#INFO: ADC Value: %d\n",adcValue);
		}
		fscanf(fp,"%d",&adcValue);
		fclose(fp);
	}
	//*********************************************************//

	//********************* PROGRAM END ***********************//
	close(socket);
	return 1;
	//*********************************************************//
}
